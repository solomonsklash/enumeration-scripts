#!/bin/bash

# Get distribution version
echo "***********************************************************"
echo "Getting version from /etc/*-release:"
cat /etc/*-release
# Get distribution version
echo "***********************************************************"
echo "Getting version from /etc/issue:"
cat /etc/issue
# get kernel version
echo "***********************************************************"
echo "Getting kernel version from /proc/version:"
cat /proc/version
echo "***********************************************************"
echo "Getting kernel version from uname -a:"
uname -a
echo "***********************************************************"
echo "Getting kernel version from uname -mrs:"
uname -mrs
echo "***********************************************************"
echo "Getting kernel version from rpm -q kernel: !!! Redhat ONLY !!!"
rpm -q kernel
echo "***********************************************************"
echo "Getting kernel version from ls /boot | grep vmlinuz-:"
ls /boot | grep vmlinuz-
echo "***********************************************************"
echo "***********************************************************"
echo "Getting installed shells from /etc/shells:"
# get environment variables & shells
cat /etc/shells
echo "***********************************************************"
if grep zsh /etc/shells 
    then echo "!!! zsh shell installed. Checking /etc/zsh/zprofile !!!"
    cat /etc/zsh/zprofile
    echo "Checking logged on user .zshrc:"
    cat ~/.zshrc
fi
echo "***********************************************************"
if grep fish /etc/shells 
    then echo "!!! fish shell installed. Checking /etc/fish/config.fish !!!"
    cat /etc/fish/config.fish
    echo "Checking logged on user~/.config/fish/config.fish:"
    cat ~/.config/fish/config.fish
fi
echo "***********************************************************"
echo "Checking Bash profile at /etc/profile"
cat /etc/profile
echo "***********************************************************"
echo "Checking system-wide .bashrc at /etc/bash.bashrc"
cat /etc/bash.bashrc
echo "***********************************************************"
echo "Checking logged on user bash settings:"
cat ~/.bash_profile
cat ~/.bashrc
cat ~/.bash_logout
echo "***********************************************************"
echo "Checking environment variables with env:"
env
echo "***********************************************************"
echo "Checking environment variables with set:"
set
echo "***********************************************************"
echo "Checking for installed printers:"
lpstat -a













